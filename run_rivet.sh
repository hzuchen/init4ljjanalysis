#!/bin/sh

# this step is only needed in case running on the batch
#cd XXXDir

#setupATLAS
source ~/.login
asetup 22.6.1,AthGeneration

source setupRivet

rivet-build RivetAnalysis_I9999999.so init4ljjAnalysis.cc

athena run_rivet.py

