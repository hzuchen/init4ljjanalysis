import re, os, glob

theApp.EvtMax = -1

evntfiles=[]
for dir in glob.glob("/eos/home-h/hzuchen/4ljj-dataset/mc16_13TeV.500372.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi_4l_m4l130.merge.EVNT.e8065_e7400/*"):  # assuming EVNT and log.generate files live in directories run_*/file.EVNT.root
    evntfiles.append(dir)


import AthenaPoolCnvSvc.ReadAthenaPool
#svcMgr.EventSelector.InputCollections = [ '/eos/home-h/hzuchen/4ljj-dataset/mc16_13TeV.500372.MGPy8EG_A14_NNPDF23LO_VBFH125_sbi_4l_m4l130.merge.EVNT.e8065_e7400/EVNT.20815506._000001.pool.root.1' ]
svcMgr.EventSelector.InputCollections = evntfiles

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses += [ 'init4ljjAnalysis' ]
rivet.RunName = ''
rivet.HistoFile = 'Rivet.yoda'
#rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True
job += rivet

from array import array
import ROOT as rt
import yoda

fName = 'Rivet.yoda'
yodaAOs = yoda.read(fName)
rtFile = rt.TFile(fName[:fName.find('.yoda')] + '.root', 'recreate')
for name in yodaAOs:
  yodaAO = yodaAOs[name];  rtAO = None
  if 'Histo1D' in str(yodaAO):
    rtAO = rt.TH1D(name, '', yodaAO.numBins(), array('d', yodaAO.xEdges()))
    rtAO.Sumw2(); rtErrs = rtAO.GetSumw2()
    for i in range(rtAO.GetNbinsX()):
      rtAO.SetBinContent(i + 1, yodaAO.bin(i).sumW())
      rtErrs.AddAt(yodaAO.bin(i).sumW2(), i+1)
  elif 'Scatter2D' in str(yodaAO):
    rtAO = rt.TGraphAsymmErrors(yodaAO.numPoints())
    for i in range(yodaAO.numPoints()):
      x = yodaAO.point(i).x(); y = yodaAO.point(i).y()
      xLo, xHi = yodaAO.point(i).xErrs()
      yLo, yHi = yodaAO.point(i).yErrs()
      rtAO.SetPoint(i, x, y)
      rtAO.SetPointError(i, xLo, xHi, yLo, yHi)
  else:
    continue
  rtAO.Write(name)
rtFile.Close()

