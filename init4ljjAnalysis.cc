// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/PromptFinalState.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class init4ljjAnalysis : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(init4ljjAnalysis);

    /// A Z Boson structrue:
    struct Z_can{
       int lep_1, lep_2;
       double dZ;
       FourMomentum p;
    };

    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections
      

      // FinalState of prompt photons and bare muons and electrons in the event
      PromptFinalState photons(Cuts::abspid == PID::PHOTON);
      PromptFinalState electrons(Cuts::abspid == PID::ELECTRON);
      PromptFinalState muons(Cuts::abspid == PID::MUON);

      // Dress the prompt bare leptons with prompt photons within dR < 0.05,
      // and apply some fiducial cuts on the dressed leptons
      Cut cuts_el = (Cuts::pT > 7*GeV) && ( Cuts::abseta < 2.47);
      Cut cuts_mu = (Cuts::pT > 5*GeV) && ( Cuts::abseta < 2.7);

      DressedLeptons dressed_electrons(photons, electrons, 0.1, cuts_el);
      declare(dressed_electrons, "DressedElectrons");

      DressedLeptons dressed_muons(photons, muons, 0.1, cuts_mu);
      declare(dressed_muons, "DressedMuons");

      // We define AntiKt4TruthWZJets here, the leptons here are not used for next step selection.
      // Muons
      PromptFinalState bare_mu(Cuts::abspid == PID::MUON, true); // true = use muons from prompt tau decays
      DressedLeptons all_dressed_mu(photons, bare_mu, 0.1, Cuts::abseta < 2.5, true);
      //
      // Electrons
      PromptFinalState bare_el(Cuts::abspid == PID::ELECTRON, true); // true = use electrons from prompt tau decays
      DressedLeptons all_dressed_el(photons, bare_el, 0.1, Cuts::abseta < 2.5, true);
      
      VetoedFinalState vfs(FinalState(Cuts::abseta < 4.5));
      // The final-state particles declared above are clustered using FastJet with
      // the anti-kT algorithm and a jet-radius parameter 0.4
      // AntiKt4TruthWZJets
      vfs.addVetoOnThisFinalState(all_dressed_el);
      vfs.addVetoOnThisFinalState(all_dressed_mu);
      FastJets jetfs(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(jetfs, "jets");

      // Book histograms
      // specify custom binning
      double Pi  = 3.141592653589793;
      book( _h["dY_SR"] , "dY_SR", {2, 3.08, 3.74, 4.32, 5.06, 7.4});
      book( _h["dY_CR"] , "dY_CR", {2, 2.94, 3.78, 5.4});
      book( _h["dphi_SR"] , "dphi_SR", {-1*Pi, -2.1,  0, 2.1, Pi});
      book( _h["dphi_CR"] , "dphi_CR", {-1*Pi, 0, Pi});
      book( _h["m_jj_SR"] , "m_jj_SR", {300, 400, 530, 720, 1080, 3280});
      book( _h["m_jj_CR"] , "m_jj_CR", {300, 410, 600, 1780});
      book( _h["m_4l_SR"] , "m_4l_SR", {130, 210, 250, 304, 400, 1130});
      book( _h["m_4l_CR"] , "m_4l_CR", {130, 226, 304, 752});
      book( _h["pt_4l_SR"] , "pt_4l_SR", {0, 50, 80, 116, 174, 512});
      book( _h["pt_4l_CR"] , "pt_4l_CR", {0, 76, 140, 424});
      book( _h["pt_jj_SR"] , "pt_jj_SR", {0, 52, 82, 116, 172, 524});
      book( _h["pt_jj_CR"] , "pt_jj_CR", {0, 80, 146, 448});
      book( _h["pt_4ljj_SR"] , "pt_4ljj_SR", {0, 20, 42, 64, 298});
      book( _h["pt_4ljj_CR"] , "pt_4ljj_CR", {0, 36, 70, 254});
      book( _h["st_4ljj_SR"] , "st_4ljj_SR", {70, 240, 320, 420, 580, 1410});
      book( _h["st_4ljj_CR"] , "st_4ljj_CR", {70, 330, 500, 1210});
      book( _h["costs1_SR"] , "cosstar1_SR", {-1, -0.5, 0, 0.5, 1});
      book( _h["costs1_CR"] , "cosstar1_CR", {-1, 0, 1});
      book( _h["costs3_SR"] , "cosstar3_SR", {-1, -0.5, 0, 0.5, 1});
      book( _h["costs3_CR"] , "cosstar3_CR", {-1, 0, 1});
      book( _h["cent_SR"] , "Centrality_SR", {0, 0.06, 0.12, 0.18, 0.26, 0.4});
      book( _h["cent_CR"] , "Centrality_CR", {0.4, 0.5, 0.64, 1.02});
      

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      // Retrieve dressed leptons, sorted by pT
      vector<DressedLepton> muons = apply<DressedLeptons>(event, "DressedMuons").dressedLeptons();
      vector<DressedLepton> elecs = apply<DressedLeptons>(event, "DressedElectrons").dressedLeptons();
      vector<DressedLepton> leps;
      leps.insert( leps.end(), muons.begin(), muons.end() );
      leps.insert( leps.end(), elecs.begin(), elecs.end() );

      size_t n_leps = leps.size();

      if (n_leps < 4) vetoEvent;
      N_baseLep++;
 
      
      //leading and sub-leading leps pt
      sort(leps.begin(), leps.end(), cmpMomByPt);
      if (leps[0].pT() < 20*GeV || leps[1].pT() < 20*GeV) vetoEvent;
      N_pTlep++;
      //lepton seperation
      double dR_min = 9999;
      for (size_t i = 0; i < n_leps - 1; i++) {
        for (size_t j = i + 1; j < n_leps; j++) {
          double dR = deltaR(leps[i], leps[j]);
          if (dR < dR_min)  dR_min = dR;     
        }
      }
      if (dR_min < 0.05) vetoEvent;
      N_dRlep++;
      

      //ZZ pairing
      size_t sfos_pair = 0;
      double minm2l    = 9999.;
      double Zmass_PDG = 91.1876;
      vector<Z_can> z;
      Particle *l1, *l3;
      for (size_t i = 0; i < n_leps - 1; i++) {
        for (size_t j = i + 1; j < n_leps; j++) {

          if (leps[i].pid() != -1*leps[j].pid())  continue; 
          sfos_pair++;
          FourMomentum twoL = leps[i].momentum() + leps[j].momentum();
          if (twoL.mass() < minm2l) minm2l = twoL.mass();

          double deltaM = fabs( twoL.mass() - Zmass_PDG );
          Z_can tmp_z;
          tmp_z.dZ = deltaM; 
          tmp_z.p  = twoL;
          if (leps[i].pid() < 0){ 
              tmp_z.lep_1 = i; 
              tmp_z.lep_2 = j;
          }else{
              tmp_z.lep_1 = j; 
              tmp_z.lep_2 = i;
          }
          z.push_back(tmp_z);
        }
      }
      if (minm2l < 5*GeV) vetoEvent; //All SFOS ll pairs should have m2l > 5 GeV.
      N_mll++;
      if (sfos_pair < 2) vetoEvent;
      N_pairs++;

      
      std::sort(z.begin(), z.end(), [](const Z_can& z1, const Z_can& z2) { return z1.dZ < z2.dZ;});
     
      int z1_id = -1;  
      int z2_id = -1;  
      FourMomentum fourL;
      bool found_4l = false;
      for (size_t i = 0; i < z.size() - 1; i++){
          for (size_t j = i + 1; j < z.size(); j++){
              if (found_4l) continue;
              if (z[i].lep_1 == z[j].lep_1 || z[i].lep_1 == z[j].lep_2 || z[i].lep_2 == z[j].lep_1 || z[i].lep_2 == z[j].lep_2) continue;
              fourL = z[i].p + z[j].p;
              if (fourL.mass() > 130*GeV){
                z1_id = i;
                z2_id = j;
                found_4l =true;
              }
          }
      }  
      if (!found_4l) vetoEvent;
      N_m4l++;

      // To judge which is the leading pair

      FourMomentum twoL_1, twoL_2;
      
      if (abs(z[z1_id].p.rap()) > abs(z[z2_id].p.rap())){
         twoL_1 = z[z1_id].p;
         twoL_2 = z[z2_id].p;
         int i = z[z1_id].lep_1;
         int j = z[z2_id].lep_1;
         l1 = &leps[i];
         l3 = &leps[j];
      }else{
         twoL_1 = z[z2_id].p;
         twoL_2 = z[z1_id].p;
         int i = z[z2_id].lep_1;
         int j = z[z1_id].lep_1;
         l1 = &leps[i];
         l3 = &leps[j];
      }

      // Retrieve clustered jets, sorted by pT, with a minimum pT cut
      Jets jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 30*GeV && Cuts::absrap < 4.5);
      if (jets.size() < 2) vetoEvent;
      N_jets++;
      if (jets[0].pt() < 40*GeV) vetoEvent;
      N_ldjets++;
      FourMomentum twoJ;
      double dY = -1;
      int jet_id1 = -1, jet_id2 = -1;
      bool found_2j = false;
      for (size_t i = 0; i < jets.size() - 1; i++){
          for (size_t j = i + 1; j < jets.size(); j++){
              if (found_2j) continue;
              if (jets[i].rap()*jets[j].rap() > 0) continue; // First pair in A/C
              dY = fabs(jets[i].rap() - jets[j].rap());
              twoJ = jets[i].momentum() + jets[j].momentum();
              jet_id1 = i;
              jet_id2 = j;
              found_2j = true;
          }
      }
      if (!found_2j) vetoEvent;
      N_acjets++;
      if (dY < 2) vetoEvent; 
      N_dy++;
      if (twoJ.mass() < 300*GeV) vetoEvent;       
      N_mjj++;   
 
      // Calculate Observables!
      
      double phi1, phi2;
      if (jets[jet_id1].rap() > jets[jet_id2].rap()) {phi1 = jets[jet_id1].phi(); phi2 = jets[jet_id2].phi();}
        else {phi1 = jets[jet_id2].phi(); phi2 = jets[jet_id1].phi();}
      double dphi = phi1 - phi2;
      double mjj  = twoJ.mass();
      dphi = mapAngleMPiToPi(dphi); 
      double m4l  = fourL.mass();
      double pt4l = fourL.pT();
      double ptjj = twoJ.pT();
      double pt4ljj = (fourL + twoJ).pT();
      double st4ljj = twoL_1.pT() + twoL_2.pT() + jets[jet_id1].pT() + jets[jet_id2].pT();
      double centrality = abs(fourL.rap() - 0.5*(jets[jet_id1].rap() + jets[jet_id2].rap())) / abs(jets[jet_id1].rap() - jets[jet_id2].rap());

      //negetive leps1 in Z rest frame work
      Vector3 beta2lcom_1 = twoL_1.betaVec();
      LorentzTransform com2lboost_1 = LorentzTransform::mkFrameTransformFromBeta(beta2lcom_1);
      FourMomentum l1_2lcom = com2lboost_1.transform(l1->momentum());
      double costs1 = cos(twoL_1.angle(l1_2lcom));


      //negetive leps3 in Z rest frame work
      Vector3 beta2lcom_2 = twoL_2.betaVec();
      LorentzTransform com2lboost_2 = LorentzTransform::mkFrameTransformFromBeta(beta2lcom_2);
      FourMomentum l3_2lcom = com2lboost_2.transform(l3->momentum());
      double costs3 = cos(twoL_2.angle(l3_2lcom));
  
      if (centrality < 0.4){ //VBS-enhanced
          N_cent++;
          _h["dphi_SR"]->fill(dphi); 
          _h["m_jj_SR"]->fill(mjj/GeV);
          _h["dY_SR"]->fill(dY);
          _h["m_4l_SR"]->fill(m4l/GeV);
          _h["pt_4l_SR"]->fill(pt4l/GeV);
          _h["pt_jj_SR"]->fill(ptjj/GeV);
          _h["pt_4ljj_SR"]->fill(pt4ljj/GeV);
          _h["st_4ljj_SR"]->fill(st4ljj/GeV);
          _h["costs1_SR"]->fill(costs1/GeV);
          _h["costs3_SR"]->fill(costs3/GeV);
          _h["cent_SR"]->fill(centrality);
      }else{ //VBS-suppressed
          _h["dphi_CR"]->fill(dphi); 
          _h["m_jj_CR"]->fill(mjj/GeV);
          _h["dY_CR"]->fill(dY);
          _h["m_4l_CR"]->fill(m4l/GeV);
          _h["pt_4l_CR"]->fill(pt4l/GeV);
          _h["pt_jj_CR"]->fill(ptjj/GeV);
          _h["pt_4ljj_CR"]->fill(pt4ljj/GeV);
          _h["st_4ljj_CR"]->fill(st4ljj/GeV);
          _h["costs1_CR"]->fill(costs1/GeV);
          _h["costs3_CR"]->fill(costs3/GeV);
          _h["cent_CR"]->fill(centrality);
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {
  
      //const double norm = crossSection()/sumOfWeights()/femtobarn; // Does not work in EFT Interference term
      const double norm = 1./femtobarn/numEvents(); // For EFT Interference term
      std::cout << "numEvent" << numEvents() << '\n';
      std::cout << "xsec: " << crossSection() << '\n';
      std::cout << "xsec/evt: " << crossSectionPerEvent() << '\n';
      std::cout << "sumw: " << sumOfWeights() << '\n';
      std::cout << "femb: " << femtobarn << '\n';
      std::cout << "norm: " << norm << '\n'; 
      std::cout << "lumifb: " << luminosityfb()  << '\n';
      std::cout << "1/event: " << norm << '\n';
      std::cout << "xsec/sumw: " << crossSection()/sumOfWeights()/femtobarn << '\n';


      scale(_h["dphi_SR"] , norm);
      scale(_h["dphi_CR"] , norm); 
      scale(_h["m_jj_SR"] , norm);
      scale(_h["m_jj_CR"] , norm);
      scale(_h["dY_SR"] , norm);
      scale(_h["dY_CR"] , norm);
      scale(_h["m_4l_SR"] , norm);
      scale(_h["m_4l_CR"] , norm);
      scale(_h["pt_4l_SR"] , norm);
      scale(_h["pt_4l_CR"] , norm);
      scale(_h["pt_jj_SR"] , norm);
      scale(_h["pt_jj_CR"] , norm);
      scale(_h["pt_4ljj_SR"] , norm);
      scale(_h["pt_4ljj_CR"] , norm);
      scale(_h["st_4ljj_SR"] , norm);
      scale(_h["st_4ljj_CR"] , norm);
      scale(_h["costs1_SR"] , norm);
      scale(_h["costs1_CR"] , norm);
      scale(_h["costs3_SR"] , norm);
      scale(_h["costs3_CR"] , norm);
      scale(_h["cent_SR"] , norm);
      scale(_h["cent_CR"] , norm);
    
     
      std::cout << "4 BaseLine leps   : " << N_baseLep << '\n'; 
      std::cout << "pT leptons > 20   : " << N_pTlep   << '\n'; 
      std::cout << "dR leptons > 0.05 : " << N_dRlep   << '\n';
      std::cout << "mll<5 SFOS pairs  : " << N_mll     << '\n';
      std::cout << "SFOS pairs > 2    : " << N_pairs   << '\n';
      std::cout << "m4l > 130         : " << N_m4l     << '\n';
      std::cout << "Num of jets >= 2  : " << N_jets    << '\n';
      std::cout << "40 GeV ld jets    : " << N_ldjets  << '\n';
      std::cout << "A/C seperation    : " << N_acjets  << '\n';
      std::cout << "dY > 2            : " << N_dy      << '\n';
      std::cout << "mjj > 300         : " << N_mjj     << '\n';
      std::cout << "Centrality > 0.4  : " << N_cent    << '\n';

    }

    private: 
     map<string, Histo1DPtr> _h;
     int N_baseLep = 0, N_pTlep = 0, N_dRlep = 0, N_mll = 0, N_pairs = 0, N_ldjets = 0, N_m4l = 0, N_jets = 0, N_acjets = 0, N_dy = 0, N_mjj = 0, N_cent = 0;
  };


  DECLARE_RIVET_PLUGIN(init4ljjAnalysis);

}
